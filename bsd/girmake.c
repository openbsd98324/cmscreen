


// Lightweight, GIR Make, not Linux, but looks like Unix.  

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h> 
#include <time.h>
#if defined(__linux__) //linux
#define OSTYPE 1
#elif defined(_WIN32)
#define OSTYPE 9
#elif defined(_WIN64)
#define OSTYPE 8
#elif defined(__unix__) 
#define OSTYPE 2
#define PATH_MAX 2500
#else
#define OSTYPE 3
#endif




int fexist(const char *a_option)
{
	char dir1[PATH_MAX]; 
	char *dir2;
	DIR *dip;
	strncpy( dir1 , "",  PATH_MAX  );
	strncpy( dir1 , a_option,  PATH_MAX  );

	struct stat st_buf; 
	int status; 
	int fileordir = 0 ; 

	status = stat ( dir1 , &st_buf);
	if (status != 0) {
		fileordir = 0;
	}
	FILE *fp2check = fopen( dir1  ,"r");
	if( fp2check ) {
		fileordir = 1; 
		fclose(fp2check);
	} 

	if (S_ISDIR (st_buf.st_mode)) {
		fileordir = 2; 
	}
	return fileordir;
}





char *strtail( char *str , int mychar )
{  
      char ptr[strlen(str)+1];
      int i,j=0;
      for( i=0; str[i]!='\0'; i++)
      {
        if ( ( str[i] != '\0' ) && ( str[i] != '\0') )
	 if ( mychar != str[ i ] ) 
           ptr[j++]=str[i];
      } 
      ptr[j]='\0';
      size_t siz = sizeof ptr ; 
      char *r = malloc( sizeof ptr );
      return r ? memcpy(r, ptr, siz ) : NULL;
}




void filecat(  char *filein , char *ccprog )
{
  int curreadpos;
  FILE *fp5;
  FILE *fp6;
  char strfooreadline[PATH_MAX];
  char strfooreadlinetmp[PATH_MAX];
  int open_tab = 0; 

    fp6 = fopen( filein , "rb");
    while( !feof(fp6) ) 
    {
          fgets(strfooreadlinetmp, PATH_MAX, fp6); 
          strncpy( strfooreadline, "" , PATH_MAX );
          for( curreadpos = 0 ; ( curreadpos <= strlen( strfooreadlinetmp ) ); curreadpos++ )
            if ( strfooreadlinetmp[ curreadpos ] != '\n' )
              strfooreadline[curreadpos]=strfooreadlinetmp[curreadpos];

	   if ( strlen( strfooreadline ) == 0 ) 
	     open_tab = 0; 

	   if ( strlen( strfooreadline ) >= 1 ) 
	   if ( strfooreadline[ 0 ] == 9 )
	   {
	       if ( open_tab == 1 )    
	       {
	         printf( "RUN: %s\n", strfooreadline ); 
		 system(  strfooreadline ); 
	       }
	   }
	   else 
	   {
	       if ( strfooreadline[strlen(strfooreadline)-1] == ':' )
	       {
		  if ( strcmp( strtail( strfooreadline , ':' ) , ccprog ) == 0 )
		  {
		    open_tab = 1; 
		  }
	       }
	   }
	 
     }
     fclose( fp6 );
     open_tab = 0; 
}








int main( int argc, char *argv[])
{
    char cwd[PATH_MAX]; int i ; 



    // Custom Makefile.fltk 
    if ( argc >= 3 )
    if ( strcmp( argv[1] , "-f" ) ==  0  ) 
    {
         printf( "Source Makefile : %s\n" , argv[ 2 ] ); 
         for( i = 1 +2  ; i < argc ; i++) 
	 {
	    printf( "\n[Command check: %s]\n" , argv[ i ] ); 
	    if ( fexist( argv[ 2 ] ) == 1 ) 
	    {
	      //filecat(  "Makefile",  argv[ i ] );
	      filecat( argv[2] ,  argv[ i ] );
	    }
	 }
	 return 0;
    }


    // Classic 
    if ( argc >= 2)
    {
          for( i = 1 ; i < argc ; i++) 
	  {
	    printf( "\n[Command check: %s]\n" , argv[ i ] ); 
	    if ( fexist( "Makefile" ) == 1 ) 
	      filecat(  "Makefile",  argv[ i ] );
	  }
          return 0;
    }
    return 0;
}






