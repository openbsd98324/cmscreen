/*
 * This file is automagically created from term.c -- DO NOT EDIT
 */

#define T_FLG 0
#define T_NUM 1
#define T_STR 2

struct term
{
  char *tcname;
  int type;
};

union tcu
{
  int flg;
  int num;
  char *str;
};

