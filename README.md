# cmscreen

CMSCREEN is an Unix fork of Screen, with fixing the compilation method and some elements.

It will compile with: 

``
 cd bsd ; 
 sh build.sh 
``

``
 cd linux ; 
 sh build.sh 
``

It is forked in order to have a single compilation method on Unix / BSD using girmake and a small base system (only CC compiler needed.)

mawk and all the automagic that does not work were removed.



